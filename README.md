## Project Description
    * This project is an assigment of the Bootcamp UI using React.

## Dependencies Installation
    
    * Move to the project folder
    * Run `npm install` if you are downloading this project for the first time.

## Development Server

    * Move to the project folder
    * Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Credentials

    * username: globant
    * password: BootcampUI

## How It Works

    * You will need to add new tasks in order to start using the application.
    * The tasks can be removed or marked as done.
        + To mark a task as done you need to double click in the desired task.
            - If you need to restore the task to the active status then double click again.
        + To delete a task you need to click the x button located at the end of the line.
    * If you reload the page, then we will ask you to login again and the tasks will be cleaned.