import React, { Component } from 'react';
import Auth from '../../helper/Auth';

// Toast
import { ToastsContainer, ToastsStore } from 'react-toasts';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            sendingRequest: false
        }
    }

    /**
     * Handle the change according the input
     * 
     * @memberof Login
     */
    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    /**
     * Login the user in the application
     * 
     * @memberof Login
     */
    login = event => {
        event.preventDefault();

        if (!this.state.username.length || !this.state.password.length) {
            ToastsStore.error('All fields are mandatory, please validate.', 5000, 'toastPosition');

            return false;
        }

        if (this.state.username !== 'globant' || this.state.password !== 'BootcampUI') {
            ToastsStore.error('These credentials do not match our records, please validate.', 5000);

            return false;
        }

        this.setState({
            sendingRequest: true
        });

        Auth.login(this.state.username);

        ToastsStore.success(`Welcome back ${this.state.username}`);

        setTimeout(() => {
            this.setState({
                sendingRequest: false
            });
            this.props.history.push('/tasks');
        }, 2000);
    }

    render() {

        return <div className="loginContainer">

            <form className="loginForm" onSubmit={this.login}>
                <h1>
                    Sign in
                </h1>
                <div className="form-content">
                    <input
                        name="username"
                        placeholder="username"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.username} />

                    <input
                        name="password"
                        placeholder="password"
                        type="password"
                        onChange={this.handleChange}
                        value={this.state.password} />

                    <button type="submit" className={this.state.sendingRequest === true ? 'button disabled' : 'button'}>
                        Log in
                    </button>

                </div>
                <ToastsContainer store={ToastsStore} />
            </form>
        </div>
    }

}

export default Login;