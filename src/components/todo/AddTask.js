import React, { Component } from 'react';

import { ToastsContainer, ToastsStore } from 'react-toasts';

class AddTask extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: ''
        }
    }

    /**
     * Handle the change according the input
     * 
     * @memberof AddTask
     */
    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    /**
     * Add a new task to the todo list object in localStorage
     * 
     * @memberof AddTask
     */
    newTask = event => {
        event.preventDefault();

        if (!this.state.title.length) {
            ToastsStore.success(`Invalid title, please try again.`, 5000);

            return false;
        }

        this.props.addNewTask(this.state.title);

        this.setState({
            title: ''
        });

    }

    render() {
        return <form onSubmit={this.newTask}>
            <input
                type="text"
                name="title"
                placeholder="Add a new todo...."
                onChange={this.handleChange}
                value={this.state.title} />

            <button type="submit" className="addBtn">Add</button>
            <ToastsContainer store={ToastsStore} />
        </form>
    }

}

export default AddTask;