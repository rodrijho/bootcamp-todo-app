import React, { Component } from 'react';
import PropTypes from 'prop-types'

class Task extends Component {

    render() {
        const { title, done, id } = this.props.task;

        return <li className={ done === true ? 'checked' : 'active' } onDoubleClick={this.props.markTaskAsDone.bind(this, id)} >
                    {title} 
                    <button onClick={this.props.deleteTask.bind( this, id )} className="close">
                        ×
                    </button> 
                </li>
    }

}

Task.propTypes  = {
    task: PropTypes.object.isRequired,
    deleteTask: PropTypes.func.isRequired,
    markTaskAsDone: PropTypes.func.isRequired
};

export default Task;