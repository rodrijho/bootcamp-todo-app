import React, { Component } from 'react';

// App Components
import Task from './Task';
import AddTask from './AddTask';

// Auth Helper
import Auth from '../../helper/Auth';

// App Styles
import '../../App.css';

class Tasks extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tasks: null
    }

  }

  /**
   * Add a new task to the todo list object in localStorage
   * 
   * @memberof Task
   */
  addNewTask = title => {
    const newTask = {
      id: this.state.tasks !== null ? this.state.tasks.length : 0,
      title: title,
      done: false
    }

    const tasks = this.state.tasks !== null ? [...this.state.tasks, newTask] : [newTask];

    this.setState({
      tasks
    });
  }

  /**
   * Delete a task from the todo list
   * 
   * @memberof Task
   */
  deleteTask = id => {
    const deleteTask = this.state.tasks.filter(task => task.id !== id);

    this.setState({
      tasks: deleteTask
    });
  }

  /**
   * Mark task as done
   * 
   * @memberof Task
   */
  markTaskAsDone = id => {
    const taskToMark = this.state.tasks.map(task => {

      if (task.id === id) {
        task.done = !task.done;
      }

      return task;

    });

    this.setState({
      tasks: taskToMark
    });

  }

  logout = () => {

    this.setState({
      tasks: null
    });
    
    Auth.logout();

    this.props.history.push('/login');
  }

  render() {

    return <section className="container">
      <section className="todoHeader">

        <div className="appName">
          <h2>My Todo List</h2>
        </div>

        <button className="logoutBtn" onClick={this.logout}>
          Logout
        </button>

      </section>

      {
        this.state.tasks !== null ?
          <ul>
            {
              this.state.tasks.map(task =>
                <Task task={task} key={task.id} deleteTask={this.deleteTask} markTaskAsDone={this.markTaskAsDone} ></Task>
              )
            }
          </ul>
          : ''
      }

      <section className="newTaskSection">
        <AddTask addNewTask={this.addNewTask} />
      </section>

    </section>
  }

}

export default Tasks;