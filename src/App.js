import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

// Auth Helper
import Auth from './helper/Auth';

// App Components
import Tasks from './components/todo/Tasks';
import Login from './components/login/Login';

// App Styles
import './App.css';

class App extends Component {

  render() {

    return (
      <BrowserRouter>

        <Switch>

          <AppPublic exact path="/" component={Login} />

          <AppPublic exact path="/login" component={Login} />

          <AppPrivate exact path="/tasks" component={Tasks} />

        </Switch>

      </BrowserRouter>
    )

  }

}

/**
 * This component will render the received component after checking if the user is authenticated.
 * 
 * @memberof App
 */
const AppPrivate = ({ component: Component }) => (
  <Route render={props => (
    Auth.isAuthenticated === true ?
      <Component {...props} /> :
      <Redirect to='/login' />
  )} />
);

/**
 * This component will render all the received component without checking the authentication
 * 
 * @memberof App
 */
const AppPublic = ({ component: Component }) => (
  <Route render={props => (
    <Component {...props} />
  )} />

);

export default App;
