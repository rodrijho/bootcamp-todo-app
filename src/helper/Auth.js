const Auth = {
    isAuthenticated: false,
    username: '',
    login(username) {
        this.isAuthenticated = true;
        this.username = username;
    },
    logout() {
        this.isAuthenticated = false;
        this.username = '';
    }
};

export default Auth;